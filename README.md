Chess program with AI implemented in .NET

![alt tag](QueemScreenshot.png)

For the AI is used advanced version of Alpha-Beta pruning with dozen of modern heuristics. 

Chess architecture is implemented on top of bitboards.

UI is implemented in WPF. All chess figures are represented as scalable graphics in WPF (Paths).
